import template1 from './navbar.html';
import controller from './navbar.controller';
import './navbar.scss';

let navbarComponent = {
  restrict: 'E',
  bindings: {},
  template: template1,
  controller,
  controllerAs: 'vm'
};

export default navbarComponent;


