class NavbarController {
  constructor(authService, $rootScope, $state) {
    this.name = 'navbar';
    let t = this;
    this.currentState = $state.current.name;
    this.currentUser = localStorage.getItem('currentUser');
    this.$rootScope = $rootScope;
    this.authService = authService;
    this.$rootScope.$on("CategoryListIsReady", function() {
      t.categories = t.getCategories(t.currentState);
    });
    this.$rootScope.$on("UsernameChange", function(event, data) {
      t.currentUser = data;
    });
    this.categories = t.getCategories(t.currentState);
    this.logged = JSON.parse(localStorage.getItem('authorized'));
    if(this.logged === true) {
      this.currentUserPic = JSON.parse(localStorage.getItem('usersList'))[this.currentUser].userPic;
    }
      }

  logOut() {
    this.authService.authorization().clear();
  }

  search(query) {
    this.$rootScope.$emit('Searching', query);
  }

  sortBy(key) {
    this.$rootScope.$emit('Sorting', key);
  }

  showCategory(category) {
    this.$rootScope.$emit('Filtering', category);
  }

  showFavorites(data) {
    this.$rootScope.$emit('Favorites', data);
  }

  getCategories(currentState) {
    if(currentState === 'store' || currentState === 'profile') {
      return JSON.parse(localStorage.getItem(currentState + '_categoryList'));
    }
  }
}
NavbarController.$inject = ['authService', '$rootScope', '$state'];
export default NavbarController;
