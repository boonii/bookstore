import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Components from './components/components';
import AppComponent from './app.component';
import Common from './common/common';
import 'normalize.css';


angular.module('app', [
    uiRouter,
    Components.name,
    Common.name,
  'angular-toArrayFilter',
    ])
  .config(($locationProvider) => {
    "ngInject";
    $locationProvider.html5Mode(true).hashPrefix('!');
  })

  .component('app', AppComponent);



/*comment*/
$("canvas").initialize( function(){
  jdenticon.update('.settings-avatar');
  jdenticon.update('.userpic');
  jdenticon.update('.avatar');
});
