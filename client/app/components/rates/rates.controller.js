
class RatesController {
  constructor(ratesService, $stateParams) {
    this.name = 'rates';
    this.ratesService = ratesService;
    this.openBookId = $stateParams.bookId;
    this.currentUser = localStorage.getItem('currentUser');
    this.showAverage();
    this.checkUserRate();
  }

  addRate(value) {
    this.ratesService.add(value, this.openBookId, this.currentUser);
    this.showAverage();
    this.checkUserRate();

  }

  showAverage() {
    this.av_rate = this.ratesService.findAverage(this.openBookId);

  }

  checkUserRate() {
    this.userRate = this.ratesService.checkUserRate(this.openBookId, this.currentUser);
  }

}

RatesController.$inject = ['ratesService', '$stateParams'];
export default RatesController;
