class RatesService {
  constructor() {
    this.rates = null;
  }

  add(value, bookId, user) {
    this.getRates(bookId);
    this.rates.push({
      'user': user,
      'rate': value
    });
    let bookData = JSON.parse(localStorage.getItem(`storeData`));
    bookData[bookId].rates = this.rates;
    localStorage.setItem('storeData', JSON.stringify(bookData));

  }

  findAverage(bookId) {
    this.getRates(bookId);
    if(this.rates.length > 0) {
      var a = 0;
      for (var i = 0; i < this.rates.length; i++) {
        a = a + this.rates[i].rate;
      }
      a = a / this.rates.length;
      this.av_rate = a.toFixed(1);
      return this.av_rate;
    }
    this.av_rate = "No rates";
    return this.av_rate;
  }

  checkUserRate(bookId, user) {
    this.getRates(bookId);
    var t = this;
    var check = this.rates.find(function(element, index, array) {
      return element.user === user;
    });
    if(check !== undefined) {
     return check.rate;
    }
  }

  getRates(bookId) {
    let currentBook = JSON.parse(localStorage.getItem('storeData'))[bookId];
    this.rates = currentBook.rates;
    return this.rates;
  }
}

export default RatesService;

