class SettingsService {
  constructor($rootScope) {
    this.getCurrentUser();
    this.$rootScope = $rootScope;
  }

  getCurrentUser() {
    this.usersList = JSON.parse(localStorage.getItem('usersList'));
    this.currentUser = localStorage.getItem("currentUser");
    this.userInfo = JSON.parse(localStorage.getItem('usersList'))[this.currentUser];
    this.currentUserPic = this.userInfo['userPic'];
    this.userWallet = this.userInfo['wallet'].toFixed(3);
    this.password = this.userInfo['password'];
  }

  topUp(input) {
    this.getCurrentUser();
    var amount = Number(input);
    this.userWallet = Number(this.userWallet) + amount;
    this.userInfo['wallet'] = this.userWallet;
    localStorage.setItem("currentUser_wallet", this.userWallet);
    this.usersList[this.currentUser] = this.userInfo;
    localStorage.setItem("usersList", JSON.stringify(this.usersList));
    $('#myModal').modal('hide');
  }

  saveChanges(login, newPassword) {
    this.getCurrentUser();
    if (login !== this.currentUser && login !== 'undefined') {
      var loginExists = this.changeUserName(login);
    }
    if (newPassword !== this.password && newPassword !== undefined) {
      this.changePassword(newPassword);
    }
    return loginExists;
  }

  changeUserName(login) {
    let t = this;
    if(t.validateLogin(login)) {
      this.loginExists = false;
      t.usersList[login] = t.usersList[t.currentUser];
      delete t.usersList[t.currentUser];
      localStorage.setItem("usersList", JSON.stringify(t.usersList));
      localStorage.setItem('currentUser', login);
      let oldUserData = localStorage.getItem(t.currentUser + '_data');
      localStorage.removeItem(t.currentUser + '_data');
      localStorage.setItem(login + '_data', oldUserData);
      t.getCurrentUser();
      this.showAlert("Username has been changed.");
      this.$rootScope.$emit('UsernameChange', login);
    }
    else {
      this.loginExists = true;
    }
    return this.loginExists;
  }

  validateLogin(login) {
    var valid = true;
    let data = JSON.parse(localStorage.getItem('usersList'));
    if (data.hasOwnProperty(login)) {
      valid = false;
    }
    return valid;
  }

  changePassword(newPassword) {
      this.usersList[this.currentUser].password = newPassword;
      localStorage.setItem("usersList", JSON.stringify(this.usersList));
      this.getCurrentUser();
      this.showAlert("Password has been changed.");

  }

  showAlert(alert) {
    $('.settings-alert').find('.change-info').html(alert);
    $('.settings-alert').fadeIn('slow', function () {
      $(this).delay(2000).fadeOut('slow');
    });
  }
}

SettingsService.$inject = ['$rootScope'];
export default SettingsService;
