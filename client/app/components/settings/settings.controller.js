
class SettingsController {
  constructor($rootScope, settingsService) {
    this.name = 'settings';
    this.settingsService = settingsService;
    this.userWallet = null;
    this.getCurrentUser();
    this.$rootScope = $rootScope;
  }
  getCurrentUser() {
    this.usersList = JSON.parse(localStorage.getItem('usersList'));
    this.currentUser = localStorage.getItem("currentUser");
    this.userInfo = JSON.parse(localStorage.getItem('usersList'))[this.currentUser];
    this.currentUserPic = this.userInfo['userPic'];
    this.userWallet = this.userInfo['wallet'].toFixed(3);
  }

  topUp(input) {
    this.settingsService.topUp(input);
    this.getCurrentUser();
  }

  saveChanges() {
    let $login = String(this.login);
    let $newPassword = this.newpassword;
    this.loginExists = this.settingsService.saveChanges($login, $newPassword);
    if(!this.loginExists) {
      this.getCurrentUser();
      this.login = this.currentUser;
      this.newpassword = undefined;
      this.confirm = undefined;
    }
  }

}
SettingsController.$inject = ['$rootScope', 'settingsService'];
export default SettingsController;
