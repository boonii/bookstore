/**
 * Created by nbosenko on 07.06.2016.
 */
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import settingsComponent from './settings.component.js';
import NavBarController from '../../common/navbar/navbar.controller';
import SettingsService from './settings.services';

let settingsModule = angular.module('settings', [
  uiRouter
])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('settings', {
        url: '/settings',
        data: {
          authorization: true,
          redirectTo: 'auth'
        },
        bindings: {
          login: '=',
          password: '=',
          confirm: '='
        },
        views: {
          'navbar': {
            controller: NavBarController,
            templateUrl: './app/common/navbar/navbar.html',
            controllerAs: 'nv'
          },
          'lower': {
            template: '<settings></settings>'
          }
        }
      });
  })

  .component('settings', settingsComponent)
  .service('settingsService', SettingsService);

export default settingsModule;
