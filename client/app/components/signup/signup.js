import angular from 'angular';
import uiRouter from 'angular-ui-router';
import signupComponent from './signup.component';
import SignupService from './signup.services';

let signupModule = angular.module('signup', [
  uiRouter
])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('signup', {
        url: '/signup',
        template: '<signup></signup>'
      });
  })


  .component('signup', signupComponent)
  .service('signupService', SignupService)
  .directive('passwordMatch', [function () {
  return {
    restrict: 'A',
    scope:true,
    require: 'ngModel',
    link: function (scope, elem , attrs,control) {
      var checker = function () {

        //get the value of the first password
        var e1 = scope.$eval(attrs.ngModel);

        //get the value of the other password
        var e2 = scope.$eval(attrs.passwordMatch);
        return e1 == e2;
      };
      scope.$watch(checker, function (n) {

        //set the form control to valid if both
        //passwords are the same, else invalid
        control.$setValidity("unique", n);
      });
    }
  };
}]);

export default signupModule;
