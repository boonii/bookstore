class SignupController {
  constructor(signupService, $scope) {
    this.name = 'signup';
    this.$scope = $scope;
    this.signupService = signupService;

  }

  onSignup() {
    let login = this.$scope.login;
    let password = this.$scope.password;
    let confirmPassword = this.$scope.confirm;
    this.signupService.signup(login, password, confirmPassword);
    this.loginExists = this.signupService.loginExists;
  }
}

SignupController.$inject = ['signupService', '$scope'];
export default SignupController;
