var md5 = require('js-md5');
class SignupService {
  constructor(authService) {
    this.authService = authService;
    this.$form = $('.form-signup');
  }

  signup(login, password) {
    if (!this.validateLogin(login)) {
      this.loginExists = true;
      return;
    }
    let data = JSON.parse(localStorage.getItem('usersList'));
    let newUser = {
      "userId": _.uniqueId('user4'),
      "password": password,
      "wallet": 0,
      "userPic": md5(login)
    };
    _.set(data, login, newUser);
    localStorage.setItem('usersList', JSON.stringify(data));
    localStorage.setItem('currentUser', login);
    localStorage.setItem('currentUser_wallet', data[login].wallet);
    this.authService.authorization().go('store');
  }

  validateLogin(login) {
    var valid = true;
    let data = JSON.parse(localStorage.getItem('usersList'));
    if (data.hasOwnProperty(login)) {
      valid = false;
    }
    return valid;
  }
}

SignupService.$inject = ['authService'];
export default SignupService;
