class StoreController {
  constructor(storeService, $rootScope) {
    this.name = 'store';
    this.storeService = storeService;
    this.getBooks();
    this.isEmpty =_.isEmpty(this.books);
    this.$rootScope = $rootScope;
    let t = this;
    this.$rootScope.$on('Searching', function(event, data) {
      t.query = data;
    });
    this.$rootScope.$on('Sorting', function(event, data) {
      t.key = data;
    });
    this.$rootScope.$on('Filtering', function(event, data) {
      t.category = data;
    });
      }

  getBooks() {
    let t = this;
      this.storeService.getBooksList().then(function(data){
        t.books = data;
        t.isEmpty =_.isEmpty(t.books);
      });

  }

  }
StoreController.$inject = ['storeService', '$rootScope'];
export default StoreController;



