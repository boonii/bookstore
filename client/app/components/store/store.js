import angular from 'angular';
import uiRouter from 'angular-ui-router';
import storeComponent from './store.component.js';
import NavBarController from '../../common/navbar/navbar.controller';
import StoreService from './store.services'

let storeModule = angular.module('store', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('store', {
      url: '/',
      views: {
        'lower': {
          template: '<store></store>'
        },
        'navbar': {
          controller: NavBarController,
          controllerAs: 'nv',
          templateUrl: './app/common/navbar/navbar_store.html'
        }

      }
    });
})

.component('store', storeComponent)
.service('storeService', StoreService);

export default storeModule;

angular.module('angular-toArrayFilter', [])

  .filter('toArray', function () {
    return function (obj, addKey) {
      if (!angular.isObject(obj)) return obj;
      if ( addKey === false ) {
        return Object.keys(obj).map(function(key) {
          return obj[key];
        });
      } else {
        return Object.keys(obj).map(function (key) {
          var value = obj[key];
          return angular.isObject(value) ?
            Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
          { $key: key, $value: value };
        });
      }
    };
  });
