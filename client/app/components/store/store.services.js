class StoreService {
  constructor($http, $rootScope, $q) {
    this.$rootScope = $rootScope;
    this.$http = $http;
    this.$q = $q;
    //this.getBooksList();
  }
  getBooksList() {
    let t = this;
    let storeData = localStorage.getItem('storeData');
    t.books = JSON.parse(storeData);
    if (t.books == null) {
      return t.books = this.$http.get('https://www.googleapis.com/books/v1/users/117830434221388092125/bookshelves/1001/volumes?maxResults=40').then(function(response){
        let data_array = response.data.items;
        t.filterBooksList(data_array);
        t.getPropertyList(data_array, 'category');
        t.$rootScope.$emit("CategoryListIsReady");
        let converted_data = {};
        data_array.forEach(function(item){
          converted_data[item.id] = item;
        });
        localStorage.setItem('storeData', JSON.stringify(converted_data));
        t.getBooksList();
        return converted_data;
      });
    }
    return this.$q.when(t.books);
  }
  filterBooksList (arr) {
    for (var i=0;i < arr.length;i++){
      var selectedProperties = {
        id: 'id',
        title: 'volumeInfo.title',
        publishedDate: 'volumeInfo.publishedDate',
        pageCount: 'volumeInfo.pageCount',
        image: 'volumeInfo.imageLinks.smallThumbnail',
        author: 'volumeInfo.authors.0',
        price: 'saleInfo.listPrice.amount',
        description: 'volumeInfo.description',
        category: 'volumeInfo.categories.0'
      };
      var A = arr[i];
      var B = {};
      Object.keys(selectedProperties).forEach( function(key){
        B[key]=selectedProperties[key].split('.').reduce(function (r, a) {
          return r && r[a];
        }, A);
      });
      arr[i] = B;
      arr[i].comments = [];
      arr[i].rates = [];
    }
    return arr;
  }

  getPropertyList(arr, property) {
    let t = this;
    var b = _.map(arr, function (o) {
      if (o[property] !== undefined) {
        return o[property];
      }
    });
    let result = _.without(_.uniq(b), undefined);
    localStorage.setItem('store_' + property + 'List', JSON.stringify(result));
  }
}

StoreService.$inject = ['$http', '$rootScope', '$q'];
export default StoreService;


/*
let t = this;
let storeData = localStorage.getItem('storeData');
t.books = JSON.parse(storeData);
if (!t.books) {
  return this.$http.get('https://www.googleapis.com/books/v1/users/117830434221388092125/bookshelves/1001/volumes?maxResults=36').then(function(response){
    if (t.books !== null) {
      return t.books;
    }
    let data_array = response.data.items;
    t.filterBooksList(data_array);
    t.getPropertyList(data_array, 'category');
    t.$rootScope.$emit("CategoryListIsReady");
    let converted_data = {};
    data_array.forEach(function(item){
      converted_data[item.id] = item;
    });
    localStorage.setItem('storeData', JSON.stringify(converted_data));
    t.getBooksList();
    return converted_data;
  });
} else {
  return t.books;
}*/
