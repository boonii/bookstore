class CommentsController {
  constructor(commentsService, $stateParams) {
    this.name = 'comments';
    this.openBookId = $stateParams.bookId;
    this.commentsService = commentsService;
    this.currentUser = localStorage.getItem('currentUser');
    this.comment = this.commentsService.getComments(this.openBookId);
    this.indexEditedComment = null;
  }

  addComment() {
    this.commentsService.addComment(this.txtcomment, this.openBookId, this.currentUser);
    this.txtcomment = "";
    this.comment = this.commentsService.getComments(this.openBookId);
      }

  removeComment($index) {
    this.commentsService.remove($index, this.openBookId, this.currentUser);
    this.comment = this.commentsService.getComments(this.openBookId);
  }

  editComment($index) {
    if (this.comment[$index].user === this.currentUser) {
      this.editing = this.comment[$index].comment;
      this.indexEditedComment = $index;
    }
  }

  saveComment(index) {
    this.commentsService.save(index, this.editing, this.openBookId);
    this.indexEditedComment = null;
    this.comment = this.commentsService.getComments(this.openBookId);

  }
}

CommentsController.$inject = ['commentsService', '$stateParams'];
export default CommentsController;
