var moment = require('moment');
class CommentsService {
  constructor() {
    this.comments = null;
  }

  addComment(comment, bookId, user) {
    this.getComments(bookId);
    this.comments.push( {
      'user': user,
      'comment': comment,
      'date': moment().format("MMM Do YYYY"),
      'sortDate': moment().format(),
      'userpic': JSON.parse(localStorage.getItem('usersList'))[user].userPic
    });
    let bookData = JSON.parse(localStorage.getItem(`storeData`));
    bookData[bookId].comments = this.comments;
    localStorage.setItem('storeData', JSON.stringify(bookData));
  }

  save(index, edits, bookId) {
    this.getComments(bookId);
    if(edits == '') {
      this.remove(index);
      return;
    }
    this.comments[index].comment = edits;
    let bookData = JSON.parse(localStorage.getItem(`storeData`));
    bookData[bookId].comments = this.comments;
    localStorage.setItem('storeData', JSON.stringify(bookData));
  }

  remove(index, bookId, user) {
    this.getComments(bookId);
    let t = this;
    let bookData = JSON.parse(localStorage.getItem(`storeData`));
    if (bookData[bookId].comments[index].user === user) {
      bookData[bookId].comments.splice(index, 1);
      this.comments = bookData[bookId].comments;
      localStorage.setItem('storeData', JSON.stringify(bookData));
    }
  }

  getComments(bookId) {
    let currentBook = JSON.parse(localStorage.getItem('storeData'))[bookId];
    this.comments = currentBook.comments;
    return this.comments;
  }
}
export default CommentsService;
