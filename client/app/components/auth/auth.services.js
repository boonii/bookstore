class AuthService {
  constructor($state) {
    this.$state = $state;
    this.saveUsers();
    this.valid = true;
  }
  saveUsers() {
    if(localStorage.getItem('usersList') === null) {
      $.getJSON('../../users/usersList.json', function(data) {
        localStorage.setItem('usersList', JSON.stringify(data))
      });
    }
  }
  login(username, password) {
    let data = JSON.parse(localStorage.getItem('usersList'));
    if (!this.validate(username, password, data)) {
      return;
    }
    localStorage.setItem('currentUser', username);
    localStorage.setItem('currentUser_wallet', data[username].wallet);
    this.authorization().go('store');
  }

  authorization () {
    let t = this;
    var authorized = Boolean(localStorage.getItem('authorized'));
    var memorizedState = null;
    var
      clear = function() {
        localStorage.removeItem('authorized');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUser_wallet');
        authorized = false;
        memorizedState = null;
        t.$state.go('auth');
      },
      go = function(fallback) {
        localStorage.setItem('authorized', 'true');
        authorized = true;
        var currentUser = localStorage.getItem('currentUser');
        if (localStorage.getItem(currentUser + '_data') === null) {
          var userData = {};
          localStorage.setItem(currentUser + '_data', JSON.stringify(userData));
        }
        var targetState = memorizedState ? memorizedState : fallback;
        t.$state.go(targetState);
      };
    return {
      authorized: authorized,
      memorizedState: memorizedState,
      clear: clear,
      go: go
    };
  }
  validate(login, password, data) {
    this.valid = true;
    if (data.hasOwnProperty(login)) {
      if(data[login].password === password) {
        this.valid = true;
        return this.valid;
      }
      this.valid = false;
    }
    this.valid = false;
    return this.valid;
  }
}

AuthService.$inject = ['$state'];
export default AuthService;
