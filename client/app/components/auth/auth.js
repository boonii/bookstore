import angular from 'angular';
import uiRouter from 'angular-ui-router';
import authComponent from './auth.component.js';
import AuthService from './auth.services';

let authModule = angular.module('auth', [
  uiRouter
])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('auth', {
        url: '/auth',
        template: '<auth></auth>'
      });
  })

  .component('auth', authComponent)
  .service('authService', AuthService)
  .run(['$rootScope', '$state', 'authService', function($rootScope, $state, authService) {

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      if (!authService.authorization().authorized) {
        if(toState.name === 'store') {
          $state.go(toState);
          authService.authorization().memorizedState = null;
          return;
        }
        if(authService.authorization().memorizedState && toState.name === 'signup') {
          $state.go(toState);
          authService.authorization().memorizedState = null;
          return;
        }
        if (authService.authorization().memorizedState && (!fromState.data || !fromState.data.redirectTo || toState.name !== fromState.data.redirectTo)) {
          authService.authorization().clear();
        }
        if (toState.data && toState.data.authorization && toState.data.redirectTo) {
          if (toState.data.memory) {
            authService.authorization().memorizedState = toState.name;          }
          $state.go(toState.data.redirectTo);
        }
      }

    });
  }]);


export default authModule;
