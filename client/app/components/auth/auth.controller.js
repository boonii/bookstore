class AuthController {

  constructor($scope, authService) {
    this.name = 'auth';
    this.authService = authService;
    this.valid = true;
    this.$scope = $scope;
      }

  onLogin() {
    let $login = this.$scope.username;
    let $password = this.$scope.password;
    this.authService.login($login, $password);
    this.valid = this.authService.valid;
  }
 }
AuthController.$inject = ['$scope', 'authService'];
export default AuthController;

