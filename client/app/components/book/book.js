import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookComponent from './book.component.js';
import CommentsController from '../comments/comments.controller';
import RatesController from '../rates/rates.controller';
import NavBarController from '../../common/navbar/navbar.controller';
import BookService from './book.services';
import CommentsService from '../comments/comments.services';
import RatesService from '../rates/rates.services';

let bookModule = angular.module('book', [
  uiRouter
])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('book', {
        url: '/books/:bookId',
        views: {
          'comments@book' : {
            templateUrl: './app/components/comments/comments.html',
            controller: CommentsController,
            controllerAs: 'cm',
            bindings: {
              txtcomment: '=',
              editing: '='
            }
          },
          'rates@book' : {
            templateUrl: './app/components/rates/rates.html',
            controller: RatesController,
            controllerAs: 'rm'

          },
          'navbar': {
            controller: NavBarController,
            controllerAs: 'nv',
            templateUrl: './app/common/navbar/navbar.html'
          },
          'lower': {
            template: '<book></book>'
          }
        }
      });
  })


  .service('bookService', BookService)
  .service('commentsService', CommentsService)
  .service('ratesService', RatesService)
  .component('book', bookComponent);

export default bookModule;
