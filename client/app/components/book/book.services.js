class BookService {
  constructor() {

  }

  getBook(bookId) {
    let bookData = JSON.parse(localStorage.getItem(`storeData`));
    let currentBook = bookData[bookId];
    return currentBook;
  }

  getBookStatus(bookId, user) {
    let isAuthorized = Boolean(JSON.parse(localStorage.getItem(`authorized`)));
    let userBookList = JSON.parse(localStorage.getItem(user + '_data'));
    let isAdded = false;
    let isFavorite = false;
    if (isAuthorized) {
      isAdded = !!userBookList[bookId] && true;
      isFavorite = !!userBookList[bookId] && userBookList[bookId].favorite === true
    }
    return {
      isFavorite,
      isAdded
    };
  }

  buy(book, user) {
    if(book.price === undefined) {
      this.add(book, user);
      return;
    }
    let currentUserWallet = localStorage.getItem('currentUser_wallet');
    if(book.price > currentUserWallet) {
      $('#bookModal').modal('show');
      return;
    }
    currentUserWallet = currentUserWallet - book.price;
    let usersList = JSON.parse(localStorage.getItem('usersList'));
    usersList[user].wallet = currentUserWallet;
    localStorage.setItem('usersList', JSON.stringify(usersList));
    localStorage.setItem('currentUser_wallet', currentUserWallet);
    this.add(book, user);
  }

  add(book, user) {
    let userData = JSON.parse(localStorage.getItem(user + '_data'));
    userData[book.id] = book;
    localStorage.setItem(user + '_data', JSON.stringify(userData));
  }

  addToFavorites(bookId, user) {
    const userData = JSON.parse(localStorage.getItem(user + '_data'));
    const book = userData[bookId];
    book.favorite = true;
    localStorage.setItem(user + '_data', JSON.stringify(userData));
  }

}

export default BookService;


