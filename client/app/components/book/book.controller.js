class BookController {
  constructor($stateParams, bookService) {
    this.$stateParams = $stateParams;
    this.name = 'book';
    this.bookService = bookService;
    this.currentUser = localStorage.getItem('currentUser');
    this.book = this.bookService.getBook(this.$stateParams.bookId);
    this.checkStatus(this.$stateParams.bookId);
  }

  checkStatus(id) {
    let statusResult = this.bookService.getBookStatus(id, this.currentUser);
    this.isAdded = statusResult.isAdded;
    this.isFavorite = statusResult.isFavorite;
  }

  buy(book) {
    this.bookService.buy(book, this.currentUser);
    this.checkStatus(this.$stateParams.bookId);
  }

  addToFavorites() {
    this.bookService.addToFavorites(this.$stateParams.bookId, this.currentUser);
    this.checkStatus(this.$stateParams.bookId);
  }

  rememberBook(id) {
    localStorage.setItem('fromBook', id);
  }
}
BookController.$inject = ['$stateParams', 'bookService'];
export default BookController;
