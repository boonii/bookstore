import angular from 'angular';
import uiRouter from 'angular-ui-router';
import profileComponent from './profile.component.js';
import NavBarController from '../../common/navbar/navbar.controller';
import ProfileService from './profile.services'

let profileModule = angular.module('profile', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('profile', {
      url: '/profile',
      data: {
        authorization: true,
        redirectTo: 'auth',
        memory: true
      },
      views: {
        'navbar': {
          controller: NavBarController,
          templateUrl: './app/common/navbar/navbar_profile.html',
          controllerAs: 'nv'
        },
        'lower': {
          template: '<profile/></profile>'
        }
      }
    });
})

.component('profile', profileComponent)
.service('profileService', ProfileService);

export default profileModule;
