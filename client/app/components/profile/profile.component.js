import template from './profile.html';
import controller from './profile.controller.js';
import './profile.scss';

let profileComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default profileComponent;
