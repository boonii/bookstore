class ProfileService {
  constructor() {
    this.currentUser = localStorage.getItem('currentUser');
  }

  getUserBooksList(user) {
    let userData = localStorage.getItem(user + '_data');
    this.books = JSON.parse(userData);
    this.getUserPropertyList(this.books, 'category');
    return this.books;
  }

  getUserPropertyList(obj, property) {
    var b = _.mapKeys(obj, function (o) {
      return o[property];
    });
    var c = _.keys(b);
    let result = _.without(_.uniq(c), undefined, 'undefined');
    localStorage.setItem('profile_' + property + 'List', JSON.stringify(result));
  }

  removeFavorite(id, user) {
    this.books[id].favorite = false;
    localStorage.setItem(user + '_data', JSON.stringify(this.books));
    this.getUserBooksList(user);
  }

  removeBook(id, user) {
    delete this.books[id];
    localStorage.setItem(user + '_data', JSON.stringify(this.books));
    this.getUserBooksList(user);
  }
}
export default ProfileService;
