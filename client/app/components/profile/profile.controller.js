class ProfileController {
  constructor(profileService, $rootScope) {
    this.name = 'profile';
    this.profileService = profileService;
    this.currentUser = localStorage.getItem('currentUser');
    this.books = this.profileService.getUserBooksList(this.currentUser);
    this.isEmpty =_.isEmpty(this.books);
    this.$rootScope = $rootScope;
    let t = this;
    this.$rootScope.$on('Searching', function(event, data) {
      t.query = data;
    });
    this.$rootScope.$on('Sorting', function(event, data) {
      t.key = data;
    });
    this.$rootScope.$on('Filtering', function(event, data) {
      t.category = data;
    });
    this.$rootScope.$on('Favorites', function(event, data) {
      t.favorite = data;
    });
  }
  removeFavorite(id) {
   this.profileService.removeFavorite(id, this.currentUser);
  }

  removeBook(id) {
    this.profileService.removeBook(id, this.currentUser);
    this.books = this.profileService.getUserBooksList(this.currentUser);
    this.isEmpty =_.isEmpty(this.books);
  }
}

ProfileController.$inject = ['profileService', '$rootScope'];
export default ProfileController;
