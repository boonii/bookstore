import angular from 'angular';
import Profile from './profile/profile';
import Store from './store/store';
import Auth from './auth/auth';
import Book from './book/book';
import Settings from './settings/settings';
import Signup from './signup/signup';
import Reader from './reader/reader';

let componentModule = angular.module('app.components', [
  Profile.name,
  Store.name,
  Auth.name,
  Book.name,
  Settings.name,
  Signup.name,
  Reader.name
]);

export default componentModule;
