class ReaderController {
  constructor() {
    this.name = 'reader';
    this.bookId = localStorage.getItem('fromBook');
    this.Book = ePub("https://s3.amazonaws.com/moby-dick/");
    this.Book.renderTo("area");
  }
}

export default ReaderController;
