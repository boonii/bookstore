import template from './reader.html';
import controller from './reader.controller';
import './reader.scss';

let readerComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};

export default readerComponent;
