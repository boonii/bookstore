import angular from 'angular';
import uiRouter from 'angular-ui-router';
import readerComponent from './reader.component';
import NavBarController from '../../common/navbar/navbar.controller';

let readerModule = angular.module('reader', [
  uiRouter
])

  .config(($stateProvider) => {
    "ngInject";
    $stateProvider
      .state('reader', {
        url: '/reader',
        data: {
          authorization: true,
          redirectTo: 'auth'
        },
        views: {
          'navbar': {
            controller: NavBarController,
            templateUrl: './app/common/navbar/navbar.html',
            controllerAs: 'nv'
          },
          'lower': {
            template: '<reader></reader>'
          }
        }
      });
  })
.component('reader', readerComponent);

export default readerModule;
